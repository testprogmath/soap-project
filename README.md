# Simple SOAP service
## Author: Anna Kh

### How to set up:
1. Install MySQL, JDK 18 and docker compose
2. Create a MySQL DB articledb (or with other name)
3. Set DB name and username/password to .env in the root of this project
4. Make sure that local port for the DB (MYSQLDB_LOCAL_PORT) and application (SPRING_LOCAL_PORT) are free. If not, change the values in .env file.
5. Go to ```cd <folder_name>``` and run:
```dockerfile
docker compose up --detach --force-recreate 
```