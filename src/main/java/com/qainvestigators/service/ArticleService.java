package com.qainvestigators.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qainvestigators.entity.Article;
import com.qainvestigators.repository.ArticleRepository;
@Service
public class ArticleService implements IArticleService {
	@Autowired
	private ArticleRepository articleRepository;
	@Override
	public Article getArticleById(long articleId) {
		return articleRepository.findByArticleId(articleId);
	}	
	@Override
	public List<Article> getAllArticles(){
		List<Article> list = new ArrayList<>();
		articleRepository.findAll().forEach(list::add);
		return list;
	}
	@Override
	public synchronized boolean addArticle(Article article){
	   List<Article> list = articleRepository.findByTitleAndCategory(article.getTitle(), article.getCategory()); 	
       if (!list.isEmpty()) {
    	   return false;
       } else {
    	   article = articleRepository.save(article);
    	   return true;
       }
	}
	@Override
	public void updateArticle(Article article) {
		articleRepository.save(article);
	}
	@Override
	public void deleteArticle(Article article) {
		articleRepository.delete(article);
	}
}
