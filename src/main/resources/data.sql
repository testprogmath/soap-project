CREATE
USER user@'localhost' IDENTIFIED BY 'cp';
GRANT UPDATE, DELETE, INSERT, SELECT
    ON articledb.*
    TO user@'localhost';